const buttons = document.querySelectorAll('.btn-wrapper .btn');
const letters = ['enter', 's','d','f','g','h','j'];
const reDye = (event) => {
    const keyPress = event.key.toLowerCase();
    const btnIndex = letters.indexOf(keyPress);

    if (btnIndex !== -1) {
        buttons.forEach((button) => (button.style.backgroundColor = "black"));
        buttons[btnIndex].style.backgroundColor = "blue";
    }
};

document.addEventListener("keydown", reDye);
